;;; Copyright © 2023 Franck Pérignon <franck.perignon@univ-grenoble-alpes.fr>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (rheolef)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 match)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages autotools)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages graphics)
  #:use-module (srfi srfi-1)
  )

(define-public ptscotch-parmetis
  (package
    (name "ptscotch-parmetis")
    (version "7.0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.inria.fr/scotch/scotch")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1fvgxd3ipl5xswswyadvxvlcgv6an8c229ispnlksgnlwphg10ig"))))
    (build-system cmake-build-system)
    (inputs
     (list zlib))
    (native-inputs
     (list flex bison))
    (propagated-inputs
     (list openmpi  gcc gfortran))
    (outputs '("out"))
    (arguments
     `(
       #:parallel-build? #t
       #:tests? #t ; disable tests
       #:configure-flags '("-DBUILD_SHARED_LIBS=YES"
                           "-DBUILD_PTSCOTCH=ON" "-DBUILD_LIBSCOTCHMETIS=ON")
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'mpi-setup
             ,%openmpi-setup))))
    (home-page "https://www.labri.fr/perso/pelegrin/scotch/")
    (properties
     `((release-monitoring-url
        . "https://gitlab.inria.fr/scotch/scotch/-/releases")))
    (synopsis "Programs and libraries for graph algorithms")
    (description "SCOTCH is a set of programs and libraries which implement
the static mapping and sparse matrix reordering algorithms developed within
the SCOTCH project.  Its purpose is to apply graph theory, with a divide and
conquer approach, to scientific computing problems such as graph and mesh
partitioning, static mapping, and sparse matrix ordering, in application
domains ranging from structural mechanics to operating systems or
bio-chemistry.")
    ;; See LICENSE_en.txt
    (license license:cecill-c)))


;; (define-public mumps-ptscotch-openmpi
;;   (package
;;    (inherit mumps-openmpi)
;;    (name "mumps-scotch32-openmpi")
;;    (inputs
;;     `(("pt-scotch" ,pt-scotch32)
;;       ,@(alist-delete "metis" (package-inputs mumps-openmpi))))))


(define-public mumps-pt-scotch-openmpi
  (package
    (name "mumps-pt-scotch-openmpi")
    (version "5.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (list (string-append "http://mumps.enseeiht.fr/MUMPS_"
                                 version ".tar.gz")
                  (string-append
                   "https://ftp.mcs.anl.gov/pub/petsc/externalpackages"
                   "/MUMPS_" version ".tar.gz")))
       (sha256
        (base32
         "05gs2i8b76m9flm1826fxpyfnwibjjawbmfza3ylrvj7zaag5gqs"))))
    (build-system gnu-build-system)
    (inputs
     (list gfortran
           ;; These are required for linking against mumps, but we let the user
           ;; declare the dependency.
           openblas
           openmpi
	   scalapack
           ptscotch-parmetis))
    (arguments
     `(#:modules ((ice-9 match)
                  (ice-9 popen)
                  (srfi srfi-1)
                  ,@%gnu-build-system-modules)
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
          (lambda* (#:key inputs outputs #:allow-other-keys)
            (call-with-output-file "Makefile.inc"
              (lambda (port)
                (format port "
PLAT          =
LIBEXT        = .a
LIBEXT_SHARED = .so
OUTC          = -o
OUTF          = -o
BLASDIR       = ~a
LIBBLAS       = -Wl,-rpath=$(BLASDIR)/lib -Wl,-rpath='$$ORIGIN'
LIBBLAS      += -L$(BLASDIR)/lib
LIBBLAS      += -lopenblas~@[
SCALAPDIR     = ~a
SCALAP        = -Wl,-rpath=$(SCALAPDIR)/lib -Wl,-rpath='$$ORIGIN'
SCALAP       += -L$(SCALAPDIR)/lib -lscalapack~]
RM            = rm -f~:[
CC            = mpicc
FC            = mpif90
FL            = mpif90
INCSEQ        = -I$(topdir)/libseq
LIBSEQ        = $(LAPACK) -L$(topdir)/libseq -lmpiseq
LIBSEQNEEDED  = libseqneeded
INCS          = $(INCSEQ)
LIBS          = $(LIBSEQ)~;
CC            = mpicc
FC            = mpifort
FL            = mpifort
INCPAR        =
LIBPAR        = $(SCALAP) $(LAPACK)
LIBSEQNEEDED  =
INCS          = $(INCPAR)
LIBS          = $(LIBPAR)~]
AR            = ar vr # rules require trailing space, ugh...
RANLIB        = ranlib
LIBOTHERS     = -pthread
CDEFS         = -DAdd_
PIC           = -fPIC
FPIC_OPT      = $(PIC)
RPATH_OPT     = -Wl,-rpath,~a/lib
OPTF          = -O2 -fopenmp -DALLOW_NON_INIT -DBLR_MT
OPTF         += -fallow-argument-mismatch $(PIC)
OPTL          = -O2 -fopenmp $(PIC)
OPTC          = -O2 -fopenmp $(PIC)
LPORDDIR      = $(topdir)/PORD/lib
IPORD         = -I$(topdir)/PORD/include
LPORD         = $(LPORDDIR)/libpord.a
ORDERINGSF    = -Dpord~@[
METISDIR      = ~a
IMETIS        = -I$(METISDIR)/include
LMETIS        = -Wl,-rpath $(METISDIR)/lib -L$(METISDIR)/lib -lmetis
ORDERINGSF   += -Dmetis~]~@[~:{
SCOTCHDIR     = ~a
ISCOTCH       = -I$(SCOTCHDIR)/include
LSCOTCH       = -Wl,-rpath $(SCOTCHDIR)/lib -L$(SCOTCHDIR)/lib ~a -lesmumps
LSCOTCH      += -lscotch -lscotcherr
ORDERINGSF   += ~a~}~]
ORDERINGSC    = $(ORDERINGSF)
LORDERINGS    = $(LPORD) $(LMETIS) $(LSCOTCH)
IORDERINGSF   = $(ISCOTCH)
IORDERINGSC   = $(IPORD) $(IMETIS) $(ISCOTCH)"
                        (assoc-ref inputs "openblas")
                        (assoc-ref inputs "scalapack")
                        (->bool (which "mpicc"))  ;; MPI support enabled?
                        (assoc-ref outputs "out")
                        (assoc-ref inputs "metis")
                        (match (list (assoc-ref inputs "ptscotch-parmetis")
                                     (assoc-ref inputs "scotch"))
                          ((#f #f)
                           #f)
                          ((#f scotch)
                           `((,scotch "" "-Dscotch")))
                          ((ptscotch _)
                           `((,ptscotch
                              "-lesmumps -lptscotch -lptscotcherr "
                              "-Dptscotch")))))))))
         (replace 'build
           ;; By default only the d-precision library is built. Make with "all"
           ;; target so that all precision libraries and examples are built.
           ;; Then, "make allshared" builts equivalent shared libraries as well.
           (lambda _
             (invoke "make" "all"
                     (format #f "-j~a" (parallel-job-count)))
             (invoke "make" "allshared"
                     (format #f "-j~a" (parallel-job-count)))))
	 (add-before 'check 'mpi-setup
	     ,%openmpi-setup)
         (replace 'check
          ;; Run the simple test drivers, which read test input from stdin:
          ;; from the "real" input for the single- and double-precision
          ;; testers, and from the "cmplx" input for complex-precision
          ;; testers.  The EXEC-PREFIX key is used by the mumps-openmpi
          ;; package to prefix execution with "mpirun".
          (lambda* (#:key (exec-prefix '("mpirun" "-n" "2")) #:allow-other-keys)
            (with-directory-excursion "examples"
              (every
               (lambda (prec type)
                 (let ((tester (apply open-pipe*
                                      `(,OPEN_WRITE
                                        ,@exec-prefix
                                        ,(string-append "./" prec
                                                        "simpletest"))))
                       (input  (open-input-file
                                (string-append "input_simpletest_" type))))
                   (begin
                     (dump-port input tester)
                     (close-port input)
                     (zero? (close-pipe tester)))))
               '("s" "d" "c" "z")
               '("real" "real" "cmplx" "cmplx")))))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (libdir (string-append out "/lib")))
               (copy-recursively "lib" libdir)
               (copy-recursively "include" (string-append out "/include"))
               (when (file-exists? "libseq/libmpiseq.a")
                 (install-file "libseq/libmpiseq.a" libdir))
               (when (file-exists? "libseq/libmpiseq.so")
                 (install-file "libseq/libmpiseq.so" libdir))
               #t))))))
    (home-page "http://mumps.enseeiht.fr")
    (synopsis "Multifrontal sparse direct solver")
    (description
     "MUMPS (MUltifrontal Massively Parallel sparse direct Solver) solves a
sparse system of linear equations A x = b using Gaussian elimination.")
    (license license:cecill-c)))

(define-public rheolef-base
  (package
    (name "rheolef-base")
    (version "7.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gricad-gitlab.univ-grenoble-alpes.fr/rheolef/rheolef.git")
             (commit "343edad9a3cc8b6b8ae7269ea832dfcc6a232587"))) ;; guix-fix branch for the moment
       (sha256
        (base32 "03lwa25wdc9xjy2nxk955lxm98bwa9plwww4n7p3gvnd0qv3p981"))))
    (build-system gnu-build-system)
    (native-inputs
     (list automake flex bison libtool))
    (inputs
     (list autoconf-wrapper gzip lbzip2))
    (propagated-inputs
     (list gcc zlib openblas eigen gmp cgal mpfr boost suitesparse))
    (arguments
     (list
       #:parallel-build? #t
       #:tests? #f ; disable tests
       #:configure-flags
       #~(list "--enable-permissive=yes"
	       "-disable-documentation"
               (string-append "--with-eigen-incdir="
                              (assoc-ref %build-inputs "eigen") "/include/eigen3")
               (string-append "--with-umfpack-incdir="
                              (assoc-ref %build-inputs "suitesparse") "/include/")
               (string-append "--with-umfpack-libdir="
                              (assoc-ref %build-inputs "suitesparse") "/lib/")
               (string-append "--with-cholmod-incdir="
                              (assoc-ref %build-inputs "suitesparse") "/include/")
               (string-append "--with-cholmod-libdir="
                         (assoc-ref %build-inputs "suitesparse") "/lib/"))
       #:phases
       #~(modify-phases %standard-phases
        	      (add-before 'bootstrap 'replace-rm
        			  (lambda _
				    (substitute* '("config/flexfix.sh"
						   "config/mk-symlink.sh")
        					 (("/bin/rm") (which "rm")))
				    (substitute* '("config/mk-symlink.sh"
						   "config/compiler.mk"
						   "config/config.mk.in")
						 (("/bin/sh") (which "bash")))
				    (substitute* '("linalg/lib/solver_cholmod.h")
						 (("suitesparse/cholmod.h") "cholmod.h"))
        	                    #t)))))
    (home-page "https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/")
    (synopsis "Efficient C++ finite element environment")
    (description
     "An efficient C++ finite element environment.")
    (license license:gpl3)))


(define-public rheolef-seq
  (package
    (inherit rheolef-base)
    (name "rheolef-seq")
    (propagated-inputs
     (modify-inputs (package-propagated-inputs rheolef-base)
		   (prepend scotch mumps)))
    (arguments
     (substitute-keyword-arguments (package-arguments rheolef-base)
       ((#:configure-flags flags)
	#~`(,(string-append "--with-scotch-incdir="
                           (assoc-ref %build-inputs "scotch") "/include/")
            ,(string-append "--with-scotch-libdir="
                           (assoc-ref %build-inputs "scotch") "/lib/")
            ,(string-append "--with-mumps-incdir="
                           (assoc-ref %build-inputs "mumps") "/include/")
            ,(string-append "--with-mumps-libdir="
                           (assoc-ref %build-inputs "mumps") "/lib/")
            ,(string-append "--with-mumps-libs=-ldmumps")
            ,(string-append "--with-scotch-libs=-lscotch -lscotcherr -lm -pthread -lz") ,@#$flags))))))


(define-public rheolef-seq-debug
  (package
    (inherit rheolef-seq)
    (name "rheolef-seq-debug")
    (arguments
     (substitute-keyword-arguments (package-arguments rheolef-seq)
       ((#:configure-flags flags)
	#~`("--enable-debug" @#$flags))))))

(define-public rheolef-parallel
  (package
    (inherit rheolef-base)
    (name "rheolef-parallel")
    (propagated-inputs
     (modify-inputs (package-propagated-inputs rheolef-base)
		    (delete "boost" )
		    (prepend openmpi openssh ptscotch-parmetis mumps-pt-scotch-openmpi boost-mpi scalapack)))
    (arguments
     (substitute-keyword-arguments (package-arguments rheolef-base)
       ((#:configure-flags flags)
	#~`("CC=mpicc" "CXX=mpicxx"
	    "--enable-mpi"
	    ,(string-append "--with-mpi-incdir="
                            (assoc-ref %build-inputs "openmpi") "/include/")
	    ,(string-append "--with-mpi-libdir="
                            (assoc-ref %build-inputs "openmpi") "/lib/")
	    ,(string-append "--with-scotch-incdir="
                            (assoc-ref %build-inputs "ptscotch-parmetis") "/include/")
	    ,(string-append "--with-scotch-libdir="
                            (assoc-ref %build-inputs "ptscotch-parmetis") "/lib/")
	    ,(string-append "--with-mumps-incdir="
                            (assoc-ref %build-inputs "mumps-pt-scotch-openmpi") "/include/")
	    ,(string-append "--with-mumps-libdir="
                            (assoc-ref %build-inputs "mumps-pt-scotch-openmpi") "/lib/")
	    ,(string-append "--with-mumps-libs=-ldmumps")
	    ,(string-append "--with-scotch-libs=-lptscotchparmetisv3 -lptscotch -lscotch -lptscotcherr -lm -pthread -lz")
	    ,(string-append "--with-scalapack-libdir="
			    (assoc-ref %build-inputs "scalapack") "/lib/")
	    ,(string-append "--with-scalapack-libs=-lscalapack")		
            ,@#$flags))
       ((#:phases phases)
        #~(modify-phases #$phases
			 (add-before 'configure 'ls-check
	                             (lambda _ (invoke "mpic++" (string-append "--showme"))
                                      (setenv "CC"  (string-append (assoc-ref %build-inputs "openmpi") "/bin/mpicc"))
                                      (setenv "CXX" (string-append (assoc-ref %build-inputs "openmpi") "/bin/mpic++"))
                                      #t))
			 (add-before 'check 'mpi-setup #$%openmpi-setup)))))))

(define-public rheolef-parallel-debug
  (package
    (inherit rheolef-parallel)
    (name "rheolef-parallel-debug")
    (arguments
     (substitute-keyword-arguments (package-arguments rheolef-parallel)
       ((#:configure-flags flags)
	#~`("--enable-debug" ,@#$flags))))))

