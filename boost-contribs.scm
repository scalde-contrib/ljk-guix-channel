;;; Copyright © 202O Franck Pérignon <franck.perignon@univ-grenoble-alpes.fr>
;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (boost-contribs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages shells)
  #:use-module (srfi srfi-1)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages boost))


;; (define-public boost-mpi
;;   (package
;;     (inherit boost)
;;     (name "boost-mpi")
;;     (version "1.72.0")
;;     (native-inputs
;;      `(("perl" ,perl)
;;        ("python" ,python-2)
;;        ("tcsh" ,tcsh)
;;        ("gcc-9" ,gcc-9)
;;        ("openmpi", openmpi)))
;;     (arguments
;;      (substitute-keyword-arguments (package-arguments boost)
;;       ((#:phases phases)
;;         `(modify-phases ,phases
;;          (add-after 'configure 'update-jam
;;            (lambda* (#:key inputs outputs #:allow-other-keys)
;;              (let ((output-port (open-file "project-config.jam" "a")))
;;                (display "using mpi ;" output-port)
;;                (newline output-port)
;;                (close output-port))))))))))


(define (version-with-underscores version)
  (string-map (lambda (x) (if (eq? x #\.) #\_ x)) version))



(define-public boost-mpi-1.67
  (package
   (inherit boost)
   (name "boost-mpi-1.67")
   (version "1.67.0")
   (source (origin
            (method url-fetch)
            (uri (string-append
                  "mirror://sourceforge/boost/boost/" version "/boost_"
                  (version-with-underscores version) ".tar.bz2"))
            (sha256
             (base32
              "1fmdlmkzsrd46wwk834jsi2ypxj68w2by0rfcg2pzrafk5rck116"))
            ))
   (build-system gnu-build-system)
   (inputs `(("icu4c" ,icu4c)
             ("zlib" ,zlib)))
   (native-inputs
    `(("perl" ,perl)
      ("python" ,python-2)
      ,@(alist-delete "python" (package-native-inputs boost))
      ("tcsh" ,tcsh)
      ("openmpi", openmpi)))
   (arguments
    (substitute-keyword-arguments (package-arguments boost)
       ((#:make-flags flags)
        `(cons* "cxxflags=-std=c++14" ,flags))
       ((#:phases phases)
        `(modify-phases ,phases
         (replace 'configure
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((icu (assoc-ref inputs "icu4c"))
                   (out (assoc-ref outputs "out")))
               (substitute* '("libs/config/configure"
                              "libs/spirit/classic/phoenix/test/runtest.sh"
                              "tools/build/doc/bjam.qbk"
                              "tools/build/src/engine/execunix.c"
                              "tools/build/src/engine/Jambase"
                              "tools/build/src/engine/jambase.c")
                 (("/bin/sh") (which "sh")))

               (setenv "SHELL" (which "sh"))
               (setenv "CONFIG_SHELL" (which "sh"))

               (invoke "./bootstrap.sh"
                       (string-append "--prefix=" out)
                       ;; Auto-detection looks for ICU only in traditional
                       ;; install locations.
                       (string-append "--with-icu=" icu)
                       "--with-toolset=gcc"))))
         
         (add-after 'configure 'update-jam
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((output-port (open-file "project-config.jam" "a")))
               (display "using mpi ;" output-port)
               (newline output-port)
               (close output-port))))
         ))))
   ))


boost-mpi-1.67
