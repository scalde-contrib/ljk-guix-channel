;;; Copyright © 2023 Franck Pérignon <franck.perignon@univ-grenoble-alpes.fr>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (json-nlohmann)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages autotools)
  #:use-module (guix build-system cmake)
)

(define-public json-nlohmann
  (package
    (name "json-nlohmann")
    (version "3.11.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/nlohmann/json.git")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0g6rfsbkvrxmacchz4kbr741yybj7mls3r4hgyfdd3pdbqhn2is9"))))
    (build-system cmake-build-system)
    (outputs '("out"))
    (arguments
     `(
       #:parallel-build? #t
       #:tests? #f ; disable tests
       ))
    (home-page "https://github.com/nlohmann/json")
    (synopsis "Json for modern C++")
    (description "Json for moderne C++")
    (license license:expat)))
