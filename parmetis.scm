;;; Copyright © 2020 Franck Pérignon <franck.perignon@univ-grenoble-alpes.fr>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parmetis)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages autotools)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages mpi))

(define-public parmetis
  (package
    (name "parmetis")
    (version "4.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/"
                           "parmetis-" version ".tar.gz"))
       (sha256
        (base32
         "0pvfpvb36djvqlcc3lq7si0c5xpb2cqndjg8wvzg35ygnwqs5ngj"))))
    (build-system cmake-build-system)
    (native-inputs `(("gcc-toolchain" ,gcc-toolchain)
                     ("openmpi" ,openmpi)))
    (arguments
     `(#:tests? #f                      ;no tests
       #:configure-flags `("-DSHARED=ON"
                           ,"-DCMAKE_C_COMPILER=mpicc"
                           ,"-DCMAKE_CXX_COMPILER=mpic++"
                           ,"-DCMAKE_VERBOSE_MAKEFILE=1"
                           ,(string-append "-DGKLIB_PATH=../parmetis-4.0.3/metis/GKlib")
                           ,(string-append "-DMETIS_PATH=../parmetis-4.0.3/metis" ))))
    (home-page "http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview")
    (synopsis "Parallel Graph Partitioning and Fill-reducing Matrix Ordering")
    (description
     "ParMETIS is an MPI-based parallel library that implements a variety of algorithms for partitioning unstructured graphs, meshes, and for computing fill-reducing orderings of sparse matrices. ParMETIS extends the functionality provided by METIS and includes routines that are especially suited for parallel AMR computations and large scale numerical simulations. The algorithms implemented in ParMETIS are based on the parallel multilevel k-way graph-partitioning, adaptive repartitioning, and parallel multi-constrained partitioning schemes developed in our lab.")
    (license license:asl2.0)))

parmetis
