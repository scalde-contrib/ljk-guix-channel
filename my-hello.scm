(define-module (my-hello)
  #:use-module (guix licenses)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages boost))

(define-public my-hello
  (package
    (name "my-hello")
    (version "2.10")
    (source (origin
	      (method url-fetch)
	      (uri (string-append "mirror://gnu/hello/hello-" version
				  ".tar.gz"))
	      (sha256
	       (base32
		"0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))
    (build-system gnu-build-system)
    (synopsis "Hello, Guix world: An example custom Guix package")
    (description
     "GNU Hello prints the message \"Hello, world!\" and then exits.  It
serves as an example of standard GNU coding practices.  As such, it supports
command-line arguments, multiple languages, and so on.")
    (home-page "https://www.gnu.org/software/hello/")
    (license gpl3+)))

my-hello
